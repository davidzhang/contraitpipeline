#!/usr/bin/env python

import os
import sys


blast_fname = "blastall.blastp"
blast_folder = "makeflow_blast"

def main():
    print "Creating database..."
    os.system("../bin/orthomclInstallSchema orthomcl.config install_schema.log")
    print "Database created!"

    if not os.path.exists("compliantFasta"):
        print "Creating compliant fasta..."
        os.mkdir("compliantFasta")
        os.system("python run_orthomclAdjustFasta.py")
    else:
        print "compliantFasta exists, skip orthomclAdjustFasta"

    if not os.path.exists("goodProteins.fasta"):
        print "Creating goodProteins.fasta..."
        os.system("../bin/orthomclFilterFasta compliantFasta 10 20")
    else:
        print "goodProteins.fasta exists, skip orthomclFilterFasta"

    if not os.path.exists(blast_fname):
        if not os.path.exists(blast_folder):
            os.mkdir(blast_folder)

        os.system("cp goodProteins.fasta %s" % blast_folder)
        os.chdir(blast_folder)
        if not os.path.exists("db"):
            os.mkdir("db")
        os.system("cp goodProteins.fasta db/db.fasta")
        os.chdir("db")
        os.system("formatdb -i db.fasta -p T")
        os.chdir("../")
        os.system("cp /afs/nd.edu/user30/wzhang7/bioinformatics/software/makeflow_blast/* ./")
        print "Generating Makeflow..."

        os.system("./makeflow_blast 500 10000 -i goodProteins.fasta -d db/db.fasta -p blastp -m 8 -e 1e-10 -o %s > Makeflow" % blast_fname)
        print "Running makeflow locally..."
        os.system("makeflow")
        os.system("mv %s ../" % blast_fname)
        os.chdir("../")
    else:
        print "Blast result exists, skip blast"

    if not os.path.exists("similarSequences.txt"):
        if os.path.exists(blast_fname):
            os.system("../bin/orthomclBlastParser %s compliantFasta > similarSequences.txt" % blast_fname)
        else:
            print "Cannot find blast output"
            sys.exit(1)
    else:
        print "similarSequences.txt exists, skip orthomclBlastParser"

    print "Importing similarSequences.txt to database..."
    command = "../bin/orthomclLoadBlast orthomcl.config similarSequences.txt"
    os.system(command)
    
    if not os.path.exists("orthomcl_pairs.log"):
        print "Running orthomcl_pairs..."
        os.system("../bin/orthomclPairs orthomcl.config orthomcl_pairs.log cleanup=no")
    else:
        print "orthomcl_pairs.log exists, skip orthomclPairs_sqlite"

    if not os.path.exists("mclInput"):
        os.system("../bin/orthomclDumpPairsFiles orthomcl.config")
    else:
        print "mclInput exists, skip orthomclDumpPairsFiles"

    if not os.path.exists("mclOutput"):
        os.system("mcl mclInput --abc -I 1.5 -o mclOutput")
    else:
        print "mclOutput exists, skip mcl"
    
    if not os.path.exists("groups.txt"):
        os.system("../bin/orthomclMclToGroups COG 1000 < mclOutput > groups.txt")
    else:
        print "groups.txt exists, skip orthomclMclToGroups"


if __name__ == "__main__":
    main()

