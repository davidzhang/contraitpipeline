#!/bin/sh

rm *.fasta
rm taxon_genome.txt
rm *.log
rm -rf compliantFasta
rm -rf makeflow_blast
rm blastall.blastp
rm similarSequences.txt
rm mclInput
rm mclOutput
rm groups.txt
rm -rf pairs

mysqldump -u weizhang -pwei-zhang --no-data wei2db | grep ^DROP | mysql -uweizhang -pwei-zhang wei2db
