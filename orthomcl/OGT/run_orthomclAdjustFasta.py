#!/usr/bin/env python

import os

compliant_folder = "compliantFasta"


def main():
    protein_folder = "/home/wzhang7/ConTraitPipeline/OGT/data/protein"
    mappingFilePath = "taxon_genome.txt"
    tax_num = 0

    mappingFile = open(mappingFilePath, "w")

    if not os.path.exists(compliant_folder):
        os.mkdir(compliant_folder)

    pro_files = os.listdir(protein_folder)
    for pfile in pro_files:
        ref_num = os.path.splitext(pfile)[0]
        mappingFile.write("%03d\t%s\n" % (tax_num, ref_num))
        pfilePath = os.path.join(protein_folder, pfile)
        cmd = "../bin/orthomclAdjustFasta %03d %s 1" % (tax_num, pfilePath)
        os.system(cmd)
        os.system("mv %03d.fasta %s" % (tax_num, compliant_folder))
        tax_num += 1

    mappingFile.close()


if __name__ == "__main__":
    main()
























