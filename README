Pipeline for genomic based functional trait study


# Requirement:

python (preferable version 2.7), perl, MySQL or sqlite (for OrthoMCL)

Installation requires root permission of the machine. If you do not have root 
permission, you need to change your local environment (~/.bashrc or ~/.cshrc) 
to install python package to your local space. cshrc.example file (my ND 
machine .cshrc file) is provided as an example. 

Install pip (https://pypi.python.org/pypi/pip), which is a tool to install and 
manage python packages.

Then, in command line, run 

pip install -r requirements.txt

which will install all required python packages for the pipeline.
Note: if you have root permission, add "root" to the above command in the beginning. 


# Prepare data

1. To start a new project, type the following command. (Optional, this step is just to 
    create a project folder sturcture. TODO: need some tweeks.)
   
   python startproject proj_name

   which will create a folder with provided proj_name, and generate the folder
   structure inside the project folder, i.e., src folder which stores the code
   files and data folder which is the warehouse for all the data (genome data,
   CDs data and protein data, etc)

2. To start regression/classification task, put genome label information into data 
   folder with file name genome_label.csv (You can change the name of this file in
   src/config.py). Each row has two columns, the first column is the genome name
   and the second column is the class label. The two columns are separated by comma.

3. Go to src folder, type the following command.
   
   python get_data.py

   which will try to download genome, cds and protein data for each genome. You
   can change the code to download from NCBI or Patricbrc FTP. Check log file to
   see failure of download and manually change genome name to re-download data.

4. type the following command in src folder:

   python preprocess_fasta.py

   to format the fasta file to the correct format required by orthomcl


# OrthoMCL

1. In orthomcl folder, create a folder for the new project (TODO: existing folder). In
   run_orthomclAdjustFasta.py file, change the value of protein_folder variable to point 
   to the location of protein folder.

2. Run the following command:

   python pipeline.py 

   which will run each step of orthomcl and generate COG result. This may require some changes 
   about the file location in the file. By default, it runs local makeflow job to blast, but you 
   can change the code to use ND distribute system by work queue. Details can be found in our 
   group bitbucket (https://bitbucket.org/NDBL/makeflow_blast).

3. After finishing, copy generated groups.txt and taxon_genome.txt files to data folder of the project


# Regression/Classification and feature selection
   
1. in src folder, run the following command:

    python util.py

   which will generate matrix.csv file in data folder. This is the data matrix we will run machine learning
   algorithm on.

2. run corresponding python script to run machine learning part and the file name is self explained.



