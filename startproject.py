#!/usr/bin/env python

import os
import sys


def usage():
    print "Usage: python startproject.py proj_name"
    sys.exit(1)

def main():
    if len(sys.argv) != 2:
        usage()

    name = sys.argv[1]
    if os.path.exists(name):
        print "project %s already exists, please use another name!" % name
        sys.exit(1)
    else:
        os.mkdir(name)
        os.mkdir("%s/data" % name)
        os.mkdir("%s/data/genome" % name)
        os.mkdir("%s/data/cds" % name)
        os.mkdir("%s/data/protein" % name)
        os.mkdir("%s/src" % name)

        # generate skeleton of config.py file
        with open("%s/src/config.py" % name, "w") as fh:
            fh.write("import os\n\n")
            fh.write("PROJECT_FOLDER = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))\n")
            fh.write("DATA_FOLDER = os.path.join(PROJECT_FOLDER, 'data')\n")
            fh.write("GENOME_FOLDER = os.path.join(DATA_FOLDER, 'genome')\n")
            fh.write("CDS_FOLDER = os.path.join(DATA_FOLDER, 'cds')\n")
            fh.write("PROTEIN_FOLDER = os.path.join(DATA_FOLDER, 'protein')\n\n")
            
            fh.write("LABEL_FPATH = os.path.join(DATA_FOLDER, 'genome_label.csv')")
            fh.write("COG_FPATH = os.path.join(DATA_FOLDER, 'groups.txt')")
            fh.write("TAXON_FPATH = os.path.join(DATA_FOLDER, 'taxon_genome.txt')")
            fh.write("MATRIX_FPATH = os.path.join(DATA_FOLDER, 'matrix.csv')")

            fh.write("TFIDF = ''")


if __name__ == "__main__":
    main()

