#!/usr/bin/env python

from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
import numpy as np
import pickle
import os
import re
import math
import operator
import random

import utils
import config


def randomforest(data, targets, tree_num=100):
    model = RandomForestRegressor(n_estimators=tree_num, 
                                  n_jobs=10,
                                  max_features=data.shape[1]/3+1,
                                  verbose=0, 
                                  oob_score=True, 
                                  compute_importances=True)

    model.fit(data, targets)
    return model


def load_model(fpath):
    return pickle.load(open(fpath))


def get_validate_data():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph_range = utils.get_genome_ph_range()

    # Get the row index of genomes with PH values in the matrix
    ph_range_genome_index = []
    for i in range(len(taxon_genome)):
        if taxon_genome[i] in genome_ph_range:
            ph_range_genome_index.append(True)
        else:
            ph_range_genome_index.append(False)

    ph_range_genome_matrix = matrix[np.array(ph_range_genome_index),:]

    targets = []
    for i in range(len(taxon_genome)):
        if ph_range_genome_index[i] == True:
            targets.append(genome_ph_range[taxon_genome[i]])
    targets = np.array(targets)

    return ph_range_genome_matrix, targets


def rf():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    #genome_ph = utils.get_genome_ph()
    genome_ph = utils.get_genome_ph("../data/complete_bacterial_ph.csv")

    ph_genome_index = []
    for i in range(len(taxon_genome)):
        if taxon_genome[i] in genome_ph:
            ph_genome_index.append(True)
        else:
            ph_genome_index.append(False)

    ph_genome_matrix = matrix[np.array(ph_genome_index),:]

    targets = []
    for i in range(len(taxon_genome)):
        if ph_genome_index[i] == True:
            targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    model = randomforest(ph_genome_matrix, targets)
    return model


def fsrf():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    #genome_ph = utils.get_genome_ph()
    genome_ph = utils.get_genome_ph("../data/complete_bacterial_ph.csv")

    ph_genome_index = []
    for i in range(len(taxon_genome)):
        if taxon_genome[i] in genome_ph:
            ph_genome_index.append(True)
        else:
            ph_genome_index.append(False)

    ph_genome_matrix = matrix[np.array(ph_genome_index),:]

    targets = []
    for i in range(len(taxon_genome)):
        if ph_genome_index[i] == True:
            targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    track_cogs = [cog for cog in cogs]
    oob_errors = []
    selected_cogs = []

    feature_num = ph_genome_matrix.shape[1]
    last_feature_num = feature_num + 1
    tmp_matrix = np.copy(ph_genome_matrix)
       
    while feature_num != last_feature_num:
        model = randomforest(tmp_matrix, targets)
        oob_rmse = math.sqrt(mean_squared_error(targets, model.oob_prediction_))
        oob_errors.append(oob_rmse)

        new_track_cogs = []
        track_index = []
        imp_cogs = []
        for ind, v in enumerate(model.feature_importances_):
            imp_cogs.append(track_cogs[ind])
            if v > 0.0:
                new_track_cogs.append(track_cogs[ind])
                track_index.append(ind)
        selected_cogs.append(imp_cogs)
            
        track_cogs = new_track_cogs
        tmp_matrix = tmp_matrix[:, track_index]
        last_feature_num = feature_num
        feature_num = len(track_cogs)

    min_index, min_value = min(enumerate(oob_errors), key=operator.itemgetter(1))

    imp_cogs = [cog for cog in selected_cogs[min_index]]

    cog_index = []
    for ind, cog in enumerate(cogs):
        if cog in imp_cogs:
            cog_index.append(ind)
            
    model = randomforest(ph_genome_matrix[:, cog_index], targets)
    return model, cog_index


def predict_range():
    model, cog_index = fsrf()
    #model = rf()
    #model = load_model("../model/rf")

    # load validation data
    val_matrix, val_targets = get_validate_data()

    predictions = model.predict(val_matrix[:, cog_index])
    
    #predictions = model.predict(val_matrix)

    count = 0
    print "Orig Range, Prediction"
    for ind, pred in enumerate(predictions):
        value = val_targets[ind]
        mat = re.match("([\d\.]+)\s*-\s*([\d\.]+).*", value)
        if mat:
            min_value = float(mat.group(1))
            max_value = float(mat.group(2))
        else:
            print "Cannot find min, max value from %s" % value

        print "%s, %f" % (value, pred)
        if pred>=min_value and pred<=max_value:
            count += 1

    print "Number in range: %d" % count


def predict_leaveout():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    targets = []
    for i in range(len(taxon_genome)):
        targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    arr = [i for i in range(len(taxon_genome))]
    random.shuffle(arr)
    remove_index = arr[:10]

    remain_index = []
    for i in range(len(taxon_genome)):
        if not i in remove_index:
            remain_index.append(i)

    sub_matrix = matrix[np.array(remain_index),:]
    sub_targets = targets[np.array(remain_index)]

    track_cogs = [cog for cog in cogs]
    oob_errors = []
    selected_cogs = []

    feature_num = matrix.shape[1]
    last_feature_num = feature_num + 1
    tmp_matrix = np.copy(sub_matrix)
       
    while feature_num != last_feature_num:
        model = randomforest(tmp_matrix, sub_targets)
        oob_rmse = math.sqrt(mean_squared_error(sub_targets, model.oob_prediction_))
        oob_errors.append(oob_rmse)

        new_track_cogs = []
        track_index = []
        imp_cogs = []
        for ind, v in enumerate(model.feature_importances_):
            imp_cogs.append(track_cogs[ind])
            if v > 0.0:
                new_track_cogs.append(track_cogs[ind])
                track_index.append(ind)
        selected_cogs.append(imp_cogs)
            
        track_cogs = new_track_cogs
        tmp_matrix = tmp_matrix[:, track_index]
        last_feature_num = feature_num
        feature_num = len(track_cogs)

    min_index, min_value = min(enumerate(oob_errors), key=operator.itemgetter(1))

    imp_cogs = [cog for cog in selected_cogs[min_index]]

    cog_index = []
    for ind, cog in enumerate(cogs):
        if cog in imp_cogs:
            cog_index.append(ind)
            
    model = randomforest(sub_matrix[:, cog_index], sub_targets)

    val_matrix = matrix[np.array(remove_index),:]
    val_targets = targets[np.array(remove_index)]
    predictions = model.predict(val_matrix[:, cog_index])
    
    print "Genome, Orig, Prediction"
    for ind, pred in enumerate(predictions):
        value = val_targets[ind]
        print "%s, %f, %f" % (taxon_genome[remove_index[ind]], value, pred)


def main():
    #predict_range()

    predict_leaveout()


if __name__ == "__main__":
    main()

