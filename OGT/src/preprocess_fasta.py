"""
This script will parse each fasta file in genome folder and change 
the sequence name. 
"""

import os
import re
from Bio import SeqIO

import config


def main():
    gfiles = os.listdir(config.PROTEIN_FOLDER)
    for gfname in gfiles:
        fpath = os.path.join(config.PROTEIN_FOLDER, gfname)
        sequences = list(SeqIO.parse(fpath, "fasta"))
        
        # If there are more than one sequences, give unique name to each sequence
        for seq in sequences:
            seq.name = ""
            seq.description = ""
            seq.id = seq.id.strip("|")
            seq.id = re.sub("\s", "", seq.id)
            seq.id = re.sub("\|", "-", seq.id)

        new_fpath = fpath + ".tmp"
        SeqIO.write(sequences, open(new_fpath, "w"), "fasta")
        os.remove(fpath)
        os.rename(new_fpath, fpath)


if __name__ == "__main__":
    main()

