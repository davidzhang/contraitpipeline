import os

PROJECT_FOLDER = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
DATA_FOLDER = os.path.join(PROJECT_FOLDER, 'data')
GENOME_FOLDER = os.path.join(DATA_FOLDER, 'genome')
CDS_FOLDER = os.path.join(DATA_FOLDER, 'cds')
PROTEIN_FOLDER = os.path.join(DATA_FOLDER, 'protein')

LABEL_FPATH = os.path.join(DATA_FOLDER, 'genome_label.csv')
COG_FPATH = os.path.join(DATA_FOLDER, 'groups.txt')
TAXON_FPATH = os.path.join(DATA_FOLDER, 'taxon_genome.txt')
MATRIX_FPATH = os.path.join(DATA_FOLDER, 'matrix.csv')

TFIDF = ''
