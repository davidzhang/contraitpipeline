#!/usr/bin/env python

from sklearn.ensemble import RandomForestRegressor
from sklearn.cross_validation import KFold
import numpy as np
import operator
import os
import math

import utils
import config
import train


def randomforest(data, targets):
    model = RandomForestRegressor(n_estimators=100, 
                                  n_jobs=10,
                                  verbose=1, 
                                  oob_score=True, 
                                  compute_importances=True)
    model.fit(data, targets)
    return model


def rf_fs():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    # Get the row index of genomes with PH values in the matrix
    ph_genome_index = []
    for i in range(len(taxon_genome)):
        if taxon_genome[i] in genome_ph:
            ph_genome_index.append(True)
        else:
            ph_genome_index.append(False)

    ph_genome_matrix = matrix[np.array(ph_genome_index),:]

    targets = []
    for i in range(len(taxon_genome)):
        if ph_genome_index[i] == True:
            targets.append(genome_ph[taxon_genome[i]])
    targets = np.array(targets)

    important_cogs = {}
    iteration = 1
    for i in range(iteration):
        model = randomforest(ph_genome_matrix, targets)
        for ind, v in enumerate(model.feature_importances_):
            if v > 0.0:
                if cogs[ind] in important_cogs:
                    important_cogs[cogs[ind]]['freq'] += 1
                    important_cogs[cogs[ind]]['importance'] += v
                else:
                    important_cogs[cogs[ind]] = {}
                    important_cogs[cogs[ind]]['freq'] = 1
                    important_cogs[cogs[ind]]['importance'] = v
    cog_importance = {}
    for cog in important_cogs:
        cog_importance[cog] = important_cogs[cog]['importance']/iteration

    sorted_cogs = sorted(cog_importance.iteritems(), key=operator.itemgetter(1), reverse=True)

    fs_result_fpath = os.path.join(config.fs_folder, "rf_%d" % iteration)
    fh = open(fs_result_fpath, "w")
    for cog, value in sorted_cogs:
        fh.write(cog + "," + str(value) + "\n")
    fh.close()


def rf_cv_fs():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    # Get the row index of genomes with PH values in the matrix
    ph_genome_index = []
    for i in range(len(taxon_genome)):
        if taxon_genome[i] in genome_ph:
            ph_genome_index.append(True)
        else:
            ph_genome_index.append(False)

    ph_genome_matrix = matrix[np.array(ph_genome_index),:]

    targets = []
    for i in range(len(taxon_genome)):
        if ph_genome_index[i] == True:
            targets.append(genome_ph[taxon_genome[i]])
    targets = np.array(targets)

    # 5 fold cross validation
    num = 1
    kf = KFold(ph_genome_matrix.shape[0], k=5)
    for train_index, test_index in kf:
        important_cogs = {}
        iteration = 1
        for i in range(iteration):
            model = randomforest(ph_genome_matrix[train_index], targets[train_index])
            for ind, v in enumerate(model.feature_importances_):
                if v > 0.0:
                    if cogs[ind] in important_cogs:
                        important_cogs[cogs[ind]]['freq'] += 1
                        important_cogs[cogs[ind]]['importance'] += v
                    else:
                        important_cogs[cogs[ind]] = {}
                        important_cogs[cogs[ind]]['freq'] = 1
                        important_cogs[cogs[ind]]['importance'] = v
        cog_importance = {}
        for cog in important_cogs:
            cog_importance[cog] = important_cogs[cog]['importance']/iteration

        sorted_cogs = sorted(cog_importance.iteritems(), key=operator.itemgetter(1), reverse=True)

        fs_result_fpath = os.path.join(config.fs_result_folder, "rf_%d_cv_%d" % (iteration, num))
        fh = open(fs_result_fpath, "w")
        for cog, value in sorted_cogs:
            fh.write(cog + "," + str(value) + "\n")
        fh.close()
        num += 1


def fs_predict(fpath):
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    # Get the row index of genomes with PH values in the matrix
    ph_genome_index = []
    for i in range(len(taxon_genome)):
        if taxon_genome[i] in genome_ph:
            ph_genome_index.append(True)
        else:
            ph_genome_index.append(False)

    ph_genome_matrix = matrix[np.array(ph_genome_index),:]

    targets = []
    for i in range(len(taxon_genome)):
        if ph_genome_index[i] == True:
            targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    ranked_cogs, scores = utils.get_cogs(fpath)
    
    ofh = open("rf_rmse.csv", "w")
    for num in range(len(cogs), len(cogs)+1):
        column_index = []
        selected_cogs = ranked_cogs[0:num]
        for cog in cogs:
            if cog in selected_cogs:
                column_index.append(True)
            else:
                column_index.append(False)
        sub_matrix = ph_genome_matrix[:, np.array(column_index)]
    
        model = train.randomforest(sub_matrix, targets)
        predictions = np.array(model.predict(sub_matrix))
        rmse = math.sqrt(np.mean((targets - predictions)**2))
        ofh.write("%d,%f\n" % (num, rmse))
    ofh.close()


def main():
    #rf_cv_fs()

    fs_fpath = os.path.join(config.fs_folder, "rf_1")
    fs_predict(fs_fpath)


if __name__ == "__main__":
    main()

