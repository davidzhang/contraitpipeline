#!/usr/bin/env python

"""
This script will read label file and download data from Patricbrc FTP 
according to genome name. Since genom may have different names, it 
may not find the data from the ftp. You need to verify according to
log file (download_data.log), change name in the label file and re-
download using the script.
"""

import re
import os
import logging

import config
import ncbi_ftp

logging.basicConfig(filename='download_data.log', 
        format='%(asctime)s %(message)s', 
        level=logging.INFO, filemode='w')

def main():
    bac_names = ncbi_ftp.get_bac_names()
    bac_dict = {}
    for bac_name in bac_names:
        bac_dict[bac_name.split('_uid')[0].strip("_")] = bac_name

    with open(config.LABEL_FPATH) as fh:
        for line in fh:
            line = line.strip()
            arr = line.split(",")
            name = arr[0]
            genome = arr[0]
            genome = re.sub("[\.\/\'\[\]\(\)]", "", genome)
            genome = re.sub("[\s():,]", "_", genome)
            #genome = re.sub("\_+", "_", genome)
            genome = re.sub("-", "_", genome)
            genome = genome.strip("_")
            
            ofpath = os.path.join(config.GENOME_FOLDER, "%s.fna" % genome)
            if os.path.exists(ofpath):
                continue
            
            if genome in bac_dict:
                bac_folder = "/genomes/Bacteria/%s" % bac_dict[genome]
                bac_files = ncbi_ftp.get_files(bac_folder)
                id = bac_files[0].split('.')[0]
                fpath = os.path.join(bac_folder, "%s.fna" % id)
                ofpath = os.path.join(config.GENOME_FOLDER, "%s.fna" % genome)
                ncbi_ftp.download_file(fpath, ofpath)
                
                fpath = os.path.join(bac_folder, "%s.ffn" % id)
                ofpath = os.path.join(config.CDS_FOLDER, "%s.ffn" % genome)
                ncbi_ftp.download_file(fpath, ofpath)
            
                fpath = os.path.join(bac_folder, "%s.faa" % id)
                ofpath = os.path.join(config.PROTEIN_FOLDER, "%s.faa" % genome)
                ncbi_ftp.download_file(fpath, ofpath)
            else:
                logging.info("Cannot find data for '%s', please check!" % name)

if __name__ == "__main__":
    main()

