#!/usr/bin/env python

import os
import re
import ftputil
import sys
import string
import time


def get_folders(path, host=None):
    """
    Get a list of folder names from ftp under path
    Return the names of folders as a list
    """
    if host is None:
        host = ftputil.FTPHost("ftp.ncbi.nlm.nih.gov", 'anonymous', 'anonymous')
    
    names = []
    host.chdir(path)
    flist = host.listdir(host.curdir) # list all files in the current directory
    for fname in flist:
        if host.path.isdir(fname):
            names.append(fname.strip())
    return names


def get_files(path, host=None, ext=None):
    """
    Get a list of file names from ftp under path, if ext is not None, then only return
    files with extension as ext.
    Return the names of files as a list
    """
    if host is None:
        host = ftputil.FTPHost("ftp.ncbi.nlm.nih.gov", 'anonymous', 'anonymous')

    names = []
    host.chdir(path)
    flist = host.listdir(host.curdir) # list all files in the current directory
    for fname in flist:
        if host.path.isfile(fname):
            if ext is not None:
                if host.path.splitext(fname)[-1] == ext:
                    names.append(fname.strip())
            else:
                names.append(fname.strip())
    return names

def download_file(fpath, output, host=None):
    if host is None:
        host = ftputil.FTPHost("ftp.ncbi.nlm.nih.gov", 'anonymous', 'anonymous')
        host.download(fpath, output)
        host.close()
    else:
        host.download(fpath, output)

def get_bac_names():
    """ Return a list of bacterial names according to NCBI FTP
    """
    host = ftputil.FTPHost("ftp.ncbi.nlm.nih.gov", 'anonymous', 'anonymous')
    fpath = "/genomes/Bacteria/"
    return get_folders(fpath, host)

def main():
    pass

if __name__ == "__main__":
    main()

