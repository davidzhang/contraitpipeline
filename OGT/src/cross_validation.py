#!/usr/bin/env python

from sklearn.ensemble import RandomForestRegressor
from sklearn.cross_validation import KFold
from sklearn.svm import SVR
from sklearn.ensemble import GradientBoostingRegressor
from sklearn import linear_model
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score

import pickle
import numpy as np
import operator
import os
import math

import utils
import config


def save_model(model, fpath):
    pickle.dump(model, open(fpath, "w"))


def randomforest(data, targets, tree_num=100):
    model = RandomForestRegressor(n_estimators=tree_num, 
                                  n_jobs=10,
                                  max_features=data.shape[1]/3+1,
                                  verbose=0, 
                                  oob_score=True, 
                                  compute_importances=True,
                                  random_state=12345678)
    model.fit(data, targets)
    return model


def svm_regression(data, targets):
    model = SVR(verbose=1, kernel='linear')
    model.fit(data, targets)
    return model


def gbr(data, targets):
    model = GradientBoostingRegressor()
    model.fit(data, targets)
    return model


def lasso(data, targets):
    model = linear_model.Lasso(alpha=0.1)
    model.fit(data, targets)
    return model


def rf_fs_cv():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    targets = []
    for i in range(len(taxon_genome)):
        targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    ofh = open("../result/fsrf_rmse_r2.csv", "w")
    
    # Iterate 50 times
    for i in range(1):
        # 5 fold cross validation
        kf = KFold(matrix.shape[0], k=5, shuffle=True)

        for train_index, test_index in kf:
            track_cogs = [cog for cog in cogs]
            oob_errors = []
            selected_cogs = []

            feature_num = matrix.shape[1]
            tmp_matrix = matrix[train_index]
        
            while feature_num > 1:
                model = randomforest(tmp_matrix, targets[train_index])
                #oob_rmse = math.sqrt(mean_squared_error(targets[test_index], model.oob_prediction_))
                cog_index = []
                for ind, cog in enumerate(cogs):
                    if cog in track_cogs:
                        cog_index.append(ind)
                predictions = model.predict(matrix[test_index][:, cog_index])
                oob_rmse = math.sqrt(mean_squared_error(targets[test_index], predictions))
                oob_errors.append(oob_rmse)
                print feature_num, oob_rmse

                index_imp = {}
                imp_cogs = []
                for ind, v in enumerate(model.feature_importances_):
                    index_imp[ind] = v
                    imp_cogs.append(track_cogs[ind])
                selected_cogs.append(imp_cogs)
        
                sorted_tuple = sorted(index_imp.iteritems(), key=operator.itemgetter(1), reverse=True)
                sorted_index = [ind for ind, value in sorted_tuple]
                feature_num /= 2
                
                track_cogs = [track_cogs[ind] for ind in sorted_index[0:feature_num]]
                tmp_matrix = tmp_matrix[:, sorted_index[0:feature_num]]
            min_index, min_value = min(enumerate(oob_errors), key=operator.itemgetter(1))
            print min_index, min_value

            imp_cogs = [cog for cog in selected_cogs[min_index]]
            print "number of cogs to use: %d" % len(imp_cogs)

            cog_index = []
            for ind, cog in enumerate(cogs):
                if cog in imp_cogs:
                    cog_index.append(ind)
            
            model = randomforest(matrix[train_index], targets[train_index])
            predictions = model.predict(matrix[test_index])
            rmse = math.sqrt(np.mean((targets[test_index] - predictions)**2))
            r2 = r2_score(targets[test_index], predictions)
            print "all: %f, %f" % (rmse, r2)
            rmse = math.sqrt(np.mean((targets[train_index] - model.oob_prediction_)**2))
            r2 = r2_score(targets[train_index], model.oob_prediction_)
            print "all oob: %f, %f" % (rmse, r2)
 
            model = randomforest(matrix[train_index][:, cog_index], targets[train_index])
            predictions = model.predict(matrix[test_index][:, cog_index])
            rmse = math.sqrt(np.mean((targets[test_index] - predictions)**2))
            r2 = r2_score(targets[test_index], predictions)
            print "fs: %f, %f" % (rmse, r2)
            rmse = math.sqrt(np.mean((targets[train_index] - model.oob_prediction_)**2))
            r2 = r2_score(targets[train_index], model.oob_prediction_)
            print "fs oob: %f, %f" % (rmse, r2)

            ofh.write("%f,%f\n" % (rmse, r2))
            ofh.flush()
    ofh.close()


def rf_cv():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    targets = []
    for i in range(len(taxon_genome)):
        targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    ofh = open("../result/rf_rmse_r2.csv", "w")
    # Iterate 50 times
    for i in range(50):
        # 5 fold cross validation
        #avg_rmse = 0.0
        #avg_r2 = 0.0
        kf = KFold(matrix.shape[0], k=5, shuffle=True)
        for train_index, test_index in kf:
            model = randomforest(matrix[train_index], targets[train_index], tree_num=100)
            predictions = model.predict(matrix[test_index])
            rmse = math.sqrt(mean_squared_error(targets[test_index], predictions))
            r2 = r2_score(targets[test_index], predictions)
            ofh.write("%f,%f\n" % (rmse, r2))
            ofh.flush()
            #avg_rmse += rmse
            #avg_r2 += r2
        #avg_rmse /= 5
        #avg_r2 /= 5
        #print "rf RMSE: %f" % avg_rmse
        #print "rf R2: %f" % avg_r2
    ofh.close()


def svm_cv():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    targets = []
    for i in range(len(taxon_genome)):
        targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    ofh = open("../result/svm_rmse_r2.csv", "w")
    # Iterate 50 times
    for i in range(50):
        # 5 fold cross validation
        #avg_rmse = 0.0
        #avg_r2 = 0.0
        kf = KFold(matrix.shape[0], k=5, shuffle=True)
        for train_index, test_index in kf:
            model = svm_regression(matrix[train_index], targets[train_index])
            predictions = model.predict(matrix[test_index])
            rmse = math.sqrt(mean_squared_error(targets[test_index], predictions))
            r2 = r2_score(targets[test_index], predictions)
            ofh.write("%f,%f\n" % (rmse, r2))
            ofh.flush()
            #avg_rmse += rmse
            #avg_r2 += r2
        #avg_rmse /= 5
        #avg_r2 /= 5
        #print "svm RMSE: %f" % avg_rmse
        #print "svm R2: %f" % avg_r2
    ofh.close()
    

def gbr_cv():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    targets = []
    for i in range(len(taxon_genome)):
        targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    ofh = open("../result/gbr_rmse_r2.csv", "w")
    # Iterate 50 times
    for i in range(50):
        # 5 fold cross validation
        #avg_rmse = 0.0
        #avg_r2 = 0.0
        kf = KFold(matrix.shape[0], k=5, shuffle=True)
        for train_index, test_index in kf:
            model = gbr(matrix[train_index], targets[train_index])
            predictions = model.predict(matrix[test_index])
            rmse = math.sqrt(mean_squared_error(targets[test_index], predictions))
            r2 = r2_score(targets[test_index], predictions)
            ofh.write("%f,%f\n" % (rmse, r2))
            ofh.flush()
            #avg_rmse += rmse
            #avg_r2 += r2
        #avg_rmse /= 5
        #avg_r2 /= 5
        #print "gbr RMSE: %f" % avg_rmse
        #print "gbr R2: %f" % avg_r2
    ofh.close()


def lasso_cv():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    targets = []
    for i in range(len(taxon_genome)):
        targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    ofh = open("../result/lasso_rmse_r2.csv", "w")
    # Iterate 50 times
    for i in range(50):
        # 5 fold cross validation
        #avg_rmse = 0.0
        #avg_r2 = 0.0
        kf = KFold(matrix.shape[0], k=5, shuffle=True)
        for train_index, test_index in kf:
            model = lasso(matrix[train_index], targets[train_index])
            predictions = model.predict(matrix[test_index])
            rmse = math.sqrt(mean_squared_error(targets[test_index], predictions))
            r2 = r2_score(targets[test_index], predictions)
            ofh.write("%f,%f\n" % (rmse, r2))
            ofh.flush()
            #avg_rmse += rmse
            #avg_r2 += r2
        #avg_rmse /= 5
        #avg_r2 /= 5

        #print "lasso RMSE: %f" % avg_rmse
        #print "lasso R2: %f" % avg_r2
    ofh.close()


def rf_remove_cv():
    cog_members = utils.get_cog_membership()
    taxon_genome = utils.get_genome_taxon()
    cogs, matrix = utils.group2matrix(cog_members, taxon_genome, config.matrix_fpath, header=False)

    genome_ph = utils.get_genome_ph()

    targets = []
    for i in range(len(taxon_genome)):
        targets.append(float(genome_ph[taxon_genome[i]]))
    targets = np.array(targets)

    ofh = open("../result/remove_rmse_r2.csv", "w")
    
    # Iterate 50 times
    for i in range(20):
        # 5 fold cross validation
        kf = KFold(matrix.shape[0], k=5, shuffle=True)

        for train_index, test_index in kf:
            track_cogs = [cog for cog in cogs]
            oob_errors = []
            selected_cogs = []

            feature_num = matrix.shape[1]
            last_feature_num = feature_num + 1
            tmp_matrix = matrix[train_index]
        
            while feature_num != last_feature_num:
                model = randomforest(tmp_matrix, targets[train_index])
                oob_rmse = math.sqrt(mean_squared_error(targets[train_index], model.oob_prediction_))
                oob_errors.append(oob_rmse)
                #print feature_num, oob_rmse

                new_track_cogs = []
                track_index = []
                imp_cogs = []
                for ind, v in enumerate(model.feature_importances_):
                    imp_cogs.append(track_cogs[ind])
                    if v > 0.0:
                        new_track_cogs.append(track_cogs[ind])
                        track_index.append(ind)
                selected_cogs.append(imp_cogs)
            
                track_cogs = new_track_cogs
                tmp_matrix = tmp_matrix[:, track_index]
                last_feature_num = feature_num
                feature_num = len(track_cogs)

            min_index, min_value = min(enumerate(oob_errors), key=operator.itemgetter(1))
            #print min_index, min_value

            imp_cogs = [cog for cog in selected_cogs[min_index]]
            #print "number of cogs to use: %d" % len(imp_cogs)

            cog_index = []
            for ind, cog in enumerate(cogs):
                if cog in imp_cogs:
                    cog_index.append(ind)
            
            model = randomforest(matrix[train_index], targets[train_index])
            predictions = model.predict(matrix[test_index])
            rmse = math.sqrt(np.mean((targets[test_index] - predictions)**2))
            r2 = r2_score(targets[test_index], predictions)
            #print "all: %f, %f" % (rmse, r2)
            rmse = math.sqrt(np.mean((targets[train_index] - model.oob_prediction_)**2))
            r2 = r2_score(targets[train_index], model.oob_prediction_)
            #print "all oob: %f, %f" % (rmse, r2)
 
            model = randomforest(matrix[train_index][:, cog_index], targets[train_index])
            predictions = model.predict(matrix[test_index][:, cog_index])
            rmse = math.sqrt(np.mean((targets[test_index] - predictions)**2))
            r2 = r2_score(targets[test_index], predictions)
            #print "fs: %f, %f" % (rmse, r2)
            rmse = math.sqrt(np.mean((targets[train_index] - model.oob_prediction_)**2))
            r2 = r2_score(targets[train_index], model.oob_prediction_)
            #print "fs oob: %f, %f" % (rmse, r2)

            ofh.write("%f,%f\n" % (rmse, r2))
            ofh.flush()
    ofh.close()


def main():
    #svm_cv()
    #gbr_cv()
    #lasso_cv()
    #rf_cv()
    #rf_fs_cv()


if __name__ == "__main__":
    main()

