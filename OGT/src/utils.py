import re
import os
import numpy
from Bio import SeqIO

import config


def get_cogs(fpath):
    """
    Get a list of COG names frm fpath, each line is a COG name 
    or COG,score format
    """
    print "Reading COGs from %s" % fpath
    cogs = list()
    scores = list()
    ifile = open(fpath, "r")
    for line in ifile:
        line = line.strip()
        if len(line)>0:
            arr = line.split(",")
            cogs.append(arr[0])
            if len(arr)>1:
                scores.append(float(arr[1]))
    ifile.close()
    return (cogs, scores)


def read_trait_data(column_name=None):
    """
    Get values of a column with name column_name
    Return: a list of values for that column
    """
    if not column_name:
        print "Please give a column name!"
        return None

    data = []
    fh = open(config.ph_fpath, "r")
    columns = fh.readline().strip().split(",") #Get header names
    try:
        index = columns.index(column_name)
    except ValueError:
        print "Cannot find %s column, please check column name!" % column_name
        return None

    for line in fh:
        line = line.strip()
        arr = line.split(",")
        value = arr[index].strip()
        data.append(value)
    fh.close()
    return data


def get_genome_label(fpath=config.LABEL_FPATH):
    """
    Read genome label values from config.LABEL_FPATH
    Return: A dict with GOLD DB id as key and PH as value
    """
    fh = open(fpath, "r")
    
    genome_label = {}
    for line in fh:
        line = line.strip()
        arr = line.split(",")
        name = arr[0]
        genome = arr[0]
        genome = re.sub("[\.\/\'\[\]\(\)]", "", genome)
        genome = re.sub("[\s():,]", "_", genome)
        genome = re.sub("-", "_", genome)
        genome = genome.strip("_")

        value = float(arr[1].strip())
        genome_label[genome] = value
    fh.close()
    
    return genome_label

def get_db_subset(output):
    fh = open(config.golddb_fpath, "r")
    header = fh.readline() # skip header

    ofh = open(output, "w")
    ofh.write(header)

    for line in fh:
        line = line.strip()
        arr = line.split(",")
        if len(arr)!=11:
            print "Wrong format, please check!"
            return

        ph = arr[-1].strip()
        if not re.match("^\d+\.?\d*$", ph):
            ofh.write(line + "\n")
        else:
            continue

    ofh.close()
    fh.close()


def group2matrix(group_members, taxon_genome, matrix_fpath=None, header=True):
    """
    Generate a matrix from COG group, each row is a species(representing with
    taxon number) and each column is a COG. If matrix_fpath is not None, write the
    matrix file, if header is set to True, then also write header information
    Return: a list of COG names/column names and a NumPy 2D array with each
    element m_ij representing the count of COG j in species i
    """
    cogs = sorted(group_members.keys())

    if (matrix_fpath is not None) and (os.path.exists(matrix_fpath)):
        print "Reading matrix from %s" % matrix_fpath
        if config.TFIDF.startswith("tfidf"):
            matrix = numpy.loadtxt(matrix_fpath, delimiter=",", dtype=numpy.float)
        else:
            matrix = numpy.loadtxt(matrix_fpath, delimiter=",", dtype=numpy.int)
    else:
        matrix = numpy.zeros((len(taxon_genome), len(group_members)), dtype=numpy.int)

        for i, cog in enumerate(cogs):
            members = group_members[cog]
            for member in members:
                arr = member.split(r"|")
                taxon = int(arr[0])
                #matrix[taxon][i] = 1 # For binary matrix
                matrix[taxon][i] += 1

        if config.TFIDF.startswith("tfidf"):
            matrix = tfidf_transform(matrix)

        if matrix_fpath is not None:
            if config.TFIDF.startswith("tfidf"):
                numpy.savetxt(matrix_fpath, matrix, delimiter=",", newline='\n', fmt="%f") 
            else:
                if header:
                    fh = open(matrix_fpath, "w")
                    fh.write("genome," + ",".join(cogs) + "\n")
                    for ind in range(len(taxon_genome)):
                        fh.write(taxon_genome[ind] + "," + ",".join(map(str, matrix[ind])) + "\n")
                    fh.close()
                else:
                    numpy.savetxt(matrix_fpath, matrix, delimiter=",", newline='\n', fmt="%d") 
    
    return (cogs, matrix)


def get_cog_membership(fpath=config.COG_FPATH):
    """
    Get all the protein membership in COGs from fpath. 
    Return a dict with COG as key and list of proteins as values
    """
    if not os.path.exists(fpath):
        print "Error: %s does not exist" % fpath
        return None

    cog_members = {}
    ifile = open(fpath, "r")
    for line in ifile:
        line = line.strip()
        arr = line.split(":")

        if len(arr) != 2:
            print "Error: format is wrong"

        cog = arr[0].strip()
        members = arr[1].strip().split()
        cog_members[cog] = members
    ifile.close()

    return cog_members


def get_genome_taxon(taxon_fpath=config.TAXON_FPATH):
    """
    Get the mapping between Goldstamp and taxon number
    Return a dict with taxon num as key and Goldstamp(GOLD DB ID) as value
    """
    taxon_genome = {}
    ifile = open(taxon_fpath, "r")
    for line in ifile:
        line = line.strip()
        if len(line)>0:
            arr = line.split()
            taxon = int(arr[0])
            genome = arr[1]
            taxon_genome[taxon] = genome
    ifile.close()
    return taxon_genome


def get_taxon_organism(fpath):
    """
    Get the taxon and organism name mapping
    Return a dict with taxon as key and organism name as value
    """
    if not os.path.exists(fpath):
        print "Error: %s does not exist" % fpath
        return
    taxon_organism = {}
    ifile = open(fpath, "r")
    for line in ifile:
        arr = line.strip().split("\t")
        taxon_organism[arr[0]] = arr[1]
    ifile.close()
    return taxon_organism


def get_cog_sequences(cog_name, cog_fpath=config.COG_FPATH, taxon_fpath=config.TAXON_FPATH):
    """
    Get a list of sequence records(Bio.SeqRecord object) in a COG
    """
    cog_members = get_cog_membership(cog_fpath)
    taxon_organism = get_taxon_organism(taxon_fpath)
    members = cog_members[cog_name]
    taxons = {}

    for member in members:
        arr = member.split("|")
        taxons[arr[0]] = 1

    taxon_sequences = {}
    for taxon in taxons:
        organism_name = taxon_organism[taxon]
        protein_fpath = os.path.join(config.protein_folder, "%s.fasta"%organism_name)
        ifile = open(protein_fpath, "rU")
        sequences = SeqIO.to_dict(SeqIO.parse(ifile, "fasta"))
        ifile.close()
        taxon_sequences[taxon] = sequences

    seq_records = []
    for member in members:
        arr = member.split("|")
        taxon = arr[0]
        orf = arr[1]
        organism_name = taxon_organism[taxon]
        seq_records.append(taxon_sequences[taxon][orf])

    return seq_records


def main():
    #get_db_subset(config.ph_fpath)

    cog_members = get_cog_membership()
    taxon_genome = get_genome_taxon()
    cogs, matrix = group2matrix(cog_members, taxon_genome, config.MATRIX_FPATH, header=True)

    #cog_statistics()


if __name__ == "__main__":
    main()

